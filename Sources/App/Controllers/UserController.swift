//
//  UserController.swift
//  App
//
//  Created by Richmond Ko on 27/06/2018.
//

import Foundation
import Vapor
import FluentSQLite
import Crypto
import Authentication

class UserController: RouteCollection {
    func boot(router: Router) throws {
        let group = router.grouped("api", "users")
        group.post(User.self, at: "register", use: registerUserHandler)
        router.get("register", use: renderRegister)
        router.post("register", use: register)
        router.get("login", use: renderLogin)
        router.get("logout", use: logout)
        
        let authSessionRouter = router.grouped(User.authSessionsMiddleware())
        authSessionRouter.post("login", use: login)
    }
}

extension UserController {
    func registerUserHandler(_ request: Request, newUser: User) throws -> Future<HTTPResponseStatus> {
        return User.query(on: request).filter(\.email == newUser.email).first().flatMap({ (existingUser) in
            guard existingUser == nil else {
                throw Abort(.badRequest, reason: "a user with this email already exists", identifier: nil)
            }
            
            let digest = try request.make(BCryptDigest.self)
            let hashedPassword = try digest.hash(newUser.password)
            let persistedUser = User(id: nil, email: newUser.email, password: hashedPassword)
            return persistedUser.save(on: request).transform(to: .created)
        })
    }
    
    func renderRegister(_ request: Request) throws -> Future<View> {
        return try request.view().render("register")
    }
    
    func register(_ request: Request) throws -> Future<Response> {
        return try request.content.decode(User.self).flatMap({ (user) in
            return try User.query(on: request).filter(\User.email == user.email).first().flatMap({ (result) in
                if let _ = result {
                    return Future.map(on: request, {
                        return request.redirect(to: "/register")
                    })
                }
                
                user.password = try BCryptDigest().hash(user.password)
                
                return user.save(on: request).map({ _ in
                    return request.redirect(to: "/login")
                })
            })
        })
    }
    
    func renderLogin(_ request: Request) throws -> Future<View> {
        return try request.view().render("login")
    }
    
    func login(_ req: Request) throws -> Future<Response> {
        return try req.content.decode(User.self).flatMap { user in
            return User.authenticate(
                username: user.email,
                password: user.password,
                using: BCryptDigest(),
                on: req
                ).map { user in
                    guard let user = user else {
                        return req.redirect(to: "/login")
                    }
                    
                    try req.authenticateSession(user)
                    return req.redirect(to: "/api/dishes")
            }
        }
    }
    
    func logout(_ req: Request) throws -> Future<Response> {
        try req.unauthenticateSession(User.self)
        return Future.map(on: req) { return req.redirect(to: "/login") }
    }
}
