//
//  DishesController.swift
//  App
//
//  Created by Richmond Ko on 27/06/2018.
//

import Foundation
import Vapor
import FluentSQLite
import Authentication

struct DishForm: Content {
    var title: String
}

class DishesController: RouteCollection {
    
    func boot(router: Router) throws {
        let basicAuthMiddleware = User.basicAuthMiddleware(using: BCrypt)
        let guardAuthMiddleware = User.guardAuthMiddleware()
        let basicAuthGroup = router.grouped([basicAuthMiddleware, guardAuthMiddleware])
        //let routeGroup = basicAuthGroup.grouped("api/dishes")
        
        let authSessionRouter = router.grouped(User.authSessionsMiddleware())
        let protectedRouter = authSessionRouter.grouped(RedirectMiddleware<User>(path: "/login")).grouped("api/dishes")
        protectedRouter.get("/", use: list)
        protectedRouter.get(Dish.parameter, use: getById)
        protectedRouter.get("courses", String.parameter, use: getByCourse)
        protectedRouter.post(Dish.self, at: "/", use: create)
        protectedRouter.post(Dish.parameter, "delete", use: deleteDish)
        protectedRouter.post(Dish.parameter, "update", use: update)
    }
    
    func list(_ request: Request) throws -> Future<View> {
        return Dish.query(on: request).all().flatMap({ (dishes) in
            let data = ["dishlist": dishes]
            return try request.view().render("crud", data)
        })
    }
    
    func getAll(request: Request) -> Future<[Dish]> {
        return Dish.query(on: request).all()
    }
    
    func getById(request: Request) throws -> Future<Dish> {
        return try request.parameters.next(Dish.self)
    }
    
    func getByCourse(request: Request) throws -> Future<[Dish]> {
        let course = try request.parameters.next(String.self).lowercased()
        return try Dish.query(on: request).filter(\.course == course).all()
    }
    
    func delete(request: Request) throws -> Future<Dish> {
        return try request.parameters.next(Dish.self).delete(on: request)
    }
    
    func deleteDish(_ request: Request) throws -> Future<Response> {
        return try request.parameters.next(Dish.self).flatMap({ (dish) in
            return dish.delete(on: request).map({ _ in
                return request.redirect(to: "/api/dishes")
            })
        })
    }
    
    func createDish(request: Request, dish: Dish) throws -> Future<Dish> {
        return dish.save(on: request)
    }
    
    func create(_ request: Request, dish: Dish) throws -> Future<Response> {
        return dish.save(on: request).map({ _ in
            return request.redirect(to: "/api/dishes")
        })
    }
    
    func update(_ request: Request) throws -> Future<Response> {
        return try request.parameters.next(Dish.self).flatMap({ (dish) in
            return try request.content.decode(DishForm.self).flatMap({ (dishForm) in
                dish.title = dishForm.title
                return dish.save(on: request).map({ _ in
                    return request.redirect(to: "/api/dishes")
                })
            })
        })
    }
    
}
