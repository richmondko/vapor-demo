import Vapor
import FluentSQLite
import Authentication
import Leaf

/// Register your application's routes here.
public func routes(_ router: Router) throws {

    let userRouterController = UserController()
    try userRouterController.boot(router: router)
    let dishesController = DishesController()
    try router.register(collection: dishesController)
    
    router.get("view") { (request) -> Future<View> in
        return try request.view().render("welcome")
    }
    
    router.get("bonus") { (request) -> Future<View> in
        let developer = Person(name: "RJ Ko", age: 28)
        return try request.view().render("whoami", developer)
    }
}

struct Person: Content {
    var name: String
    var age: Int
}
