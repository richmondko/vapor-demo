//
//  User.swift
//  App
//
//  Created by Richmond Ko on 27/06/2018.
//

import Foundation
import Vapor
import Fluent
import FluentSQLite
import Authentication

final class User: Content, SQLiteUUIDModel, Migration {
    var id: UUID?
    var email: String
    var password: String
    
    init(id: UUID? = nil, email: String, password: String) {
        self.id = id
        self.email = email
        self.password = password
    }
}

extension User: PasswordAuthenticatable {
    static var usernameKey: WritableKeyPath<User, String> {
        return \User.email
    }
    
    static var passwordKey: WritableKeyPath<User, String> {
        return \.password
    }
}

extension User: SessionAuthenticatable {}
